const nextJest = require('next/jest');

const createConfig = ({ dir }) => async (baseConfig) => {
  const { moduleNameMapper, ...rest } = baseConfig;
  const config = await nextJest({ dir })(rest)()
  return {
    ...config,
    moduleNameMapper: { ...moduleNameMapper, ...config.moduleNameMapper }
  }
}

/** @type {() => Promise<any>} */
module.exports = createConfig({ dir: './' })({
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.{js,jsx,ts,tsx}',
  ],
  coverageDirectory: 'coverage',
  moduleNameMapper: {
    '^lodash-es$': 'lodash',
    '\\.svg$': '<rootDir>/test/mocks/svg',
  },
  setupFilesAfterEnv: [
    '<rootDir>/jest/setup.js',
  ],
  testRegex: [
    '\/test\/(?!mocks\/).*\.[jt]s?x',
  ],
  testEnvironment: 'jest-environment-jsdom',
  transform: {
    '\\.js$': '<rootDir>/jest/transform.js',
  },
  transformIgnorePatterns: [
    '!node_modules/',
  ]
});
