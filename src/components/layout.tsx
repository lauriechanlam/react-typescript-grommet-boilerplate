import {
  Anchor,
  Box,
  Grommet,
  Header,
  Menu,
  ResponsiveContext,
} from 'grommet';

import Link from 'next/link';
import React from 'react';
import { useTranslation } from 'react-i18next';
import theme from '../themes/base';

type LayoutProps = {
  children: React.ReactNode;
};

function NavHeader() {
  return (
    <Header background="dark-1" pad="medium">
      <Box direction="row" align="center" gap="small">
        Title in the Nav
      </Box>
      <ResponsiveContext.Consumer>
        {(responsive) => (responsive === 'small' ? (
          <Menu
            label="Menu"
            items={[
              { label: 'Menu Item 1', onClick: () => {} },
              { label: 'Menu Item 2', onClick: () => {} },
              { label: 'Menu Item 3', onClick: () => {} },
            ]}
          />
        ) : (
          <Box direction="row">
            <Anchor href="#" label="Menu Item 1" />
            <Anchor href="#" label="Menu Item 2" />
            <Anchor href="#" label="Menu Item 3" />
          </Box>
        ))}
      </ResponsiveContext.Consumer>
    </Header>
  );
}

function Layout({ children }: LayoutProps) {
  const { t: translation } = useTranslation();
  return (
    <Grommet
      theme={theme}
      full
    >
      <NavHeader />
      <Link href="/" passHref>
        <h1>
          {translation('link-to-home-page')}
        </h1>
      </Link>
      {children}
    </Grommet>
  );
}

export default Layout;
