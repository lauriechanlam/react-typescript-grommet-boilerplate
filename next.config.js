/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  webpack: (config) => {
    config.module.rules = [...config.module.rules, 
      {
        test: /\.svg$/i,
        use: '@svgr/webpack',
      },
    ];
    return config;
  }
}
