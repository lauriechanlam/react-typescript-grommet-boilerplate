import { Spinner } from 'grommet';
import React, { Suspense } from 'react';

const Home = React.lazy(() => import('../src/components/home'));

export default function HomePage() {
  return <Suspense fallback={<Spinner />}><Home /></Suspense>;
}
