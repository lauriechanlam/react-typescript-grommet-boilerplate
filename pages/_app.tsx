import React from 'react';
import type { AppProps } from 'next/app';
import '../src/i18n/i18n';

function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}

export default App;
