import { Box } from 'grommet';
import React from 'react';

export default function Svg({ fill, height, width }: { fill?: string; height?: string; width?: string }) {
  return <Box background={fill} height={height} width={width} />;
}

Svg.defaultProps = {
  fill: undefined,
  height: undefined,
  width: undefined,
};
