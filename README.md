# React Typescript Grommet Boilerplate

## What is this?

This is the code source for several web apps using
[React](https://reactjs.org/),
[TypeScript](https://www.typescriptlang.org/) and
[Grommet](https://v2.grommet.io/)


## Install

Install the following tools:
* [Yarn](https://classic.yarnpkg.com/lang/en/docs/install)
* [Docker](https://docs.docker.com/install/)

Run the following command in your Terminal
```console
yarn install
```


## Run

### Development mode

Run `yarn dev`

### Production mode

Build the project by running `yarn build`, then run the server by running `yarn start`.

### Docker

You can also build and run the project in Docker by running `yarn build:docker` then `yarn start:docker`. You can stop your Docker container by running `yarn stop:docker`.

### Deploy to AWS

#### Install the AWS Elastic Beanstalk CLI

```sh
apt-get install -y curl
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
pip install awsebcli --upgrade --user
```

Add `/root/.local/bin` to your path in `~/.bashrc` or `~/.zshrc` file by adding the following line:
`export PATH=/root/.local/bin:$PATH`

#### Create User Profile

Create [AWS Access Keys](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html)
and add the following lines to `~/.aws/config` (create the file if need be, and use the acutal values)

```sh
[profile eb-cli]
aws_access_key_id = $AWS_ACCESS_KEY_ID
aws_secret_access_key = $AWS_SECRET_ACCESS_KEY
```

#### Create the AWS Elastic Beanstalk Application

```sh
eb init
```

Replace `application-name` by the value entered while initialising Elastic Beanstalk in the `.elasticbeanstalk/config.yml` file.


#### Deploy manually

```sh
eb deploy $AWS_DEFAULT_ENVIRONMENT --region $AWS_DEFAULT_REGION  # Replace with actual values
```

#### Deploy via GitLab

Set the following variables in [GitLab](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)

```sh
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_DEFAULT_ENVIRONMENT
AWS_DEFAULT_REGION
```

Remove the `when: manual` line in the `.gitlab-ci.yml` file

## Thanks

Based on [this tutorial](https://www.freecodecamp.org/news/learn-webpack-for-react-a36d4cac5060/)
the the basic styling is from [Grommet designer](https://designer.grommet.io/)
